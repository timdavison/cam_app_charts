<?php
/**
 * @file
 * The data_chart class.
 */

class ChartDataChart {

  // class variables.
  private $app_data = array();
  private $off_data = array();
  private $acc_data = array();
  private $label_data = array();
  private $records = array();
  private $title;

  /**
   * Constructor.
   */
  public function __construct($results) {
    $this->records = $results;
  }

  /*
  * Methods
  */
  public function set_title($param) {
    $this->title = $param;
  }

  public function render_chart() {

    foreach ($this->records as $record) {
      $this->app_data[] = (int)$record->sumtap;
      $this->off_data[] = (int)$record->sumtof;
      $this->acc_data[] = (int)$record->sumtac;
      $this->label_data[] = $record->x_axis;
    }

    $chart = array(
      '#type' => 'chart',
      '#chart_type' => 'column',
      '#title' => $this->title,
      '#height' => 600,
      '#width' => 1280,
    );
    $chart['applications'] = array(
      '#type' => 'chart_data',
      '#title' => t('Total application'),
      '#data' => $this->app_data,
      '#suffix' => '',
    );
    $chart['offers'] = array(
      '#type' => 'chart_data',
      '#title' => t('Total offers'),
      '#data' => $this->off_data,
      '#suffix' => '',
    );
      $chart['acceptances'] = array(
      '#type' => 'chart_data',
      '#title' => t('Total acceptances'),
      '#data' => $this->acc_data,
      '#suffix' => '',
    );
    $chart['xaxis'] = array(
      '#type' => 'chart_xaxis',
      '#labels' => $this->label_data,
    );

    return drupal_render($chart);
  }

  public function render_table() {

  }

}
