<?php
/**
 * @file
 * The data grabber class.
 */

class ChartDataGrabber {

  // class variables.
  private $type;
  private $year;
  private $what;

  private $for_colleges = array();
  private $for_courses = array();
  private $course;
  private $college;
  private $with_group;

  private $applications;
  private $direct_apps;
  private $open_apps;

  private $offers;
  private $direct_offers;
  private $winter_pool_offers;
  private $other_pool_offers;

  private $acceptances;
  private $college_acc;
  private $summer_pool_acc;

  /**
   * Constructor.
   */
  public function __construct($period) {

    $this->type = $period;
    $this->what = 'university';
    $this->year = '2005';

    $this->for_colleges = array(1 => 'jesus college',);
    $this->for_courses = array(1 => 'classics', );
    $this->college = 'jesus college';
    $this->course = 'classics';
    $this->with_group = 'field_college_value';

    $this->applications = true;
    $this->direct_apps = false;
    $this->open_apps = false;

    $this->offers = true;
    $this->direct_offers = false;
    $this->winter_pool_offers = false;
    $this->other_pool_offers = false;

    $this->acceptances = true;
    $this->college_acc = false;
    $this->summer_pool_acc = false;

  }

  /*
  * Methods
  */
  public function grabdata() {

    $query = db_select('eck_application_statistic', 'a');                    // select the main table
    $query->join('field_data_field_college', 'col', 'a.id = col.entity_id');     // join other tables for fields
    $query->join('field_data_field_course', 'cor', 'a.id = cor.entity_id');
    $query->join('field_data_field_ucas_year', 'u', 'a.id = u.entity_id');
    $query->join('field_data_field_total_applications', 'tap', 'a.id = tap.entity_id');
    $query->join('field_data_field_total_offers', 'tof', 'a.id = tof.entity_id');
    $query->join('field_data_field_total_acceptances', 'tac', 'a.id = tac.entity_id');

    if( $this->type == 'year') { // A single year

      if($this->with_group == 'college') {
        $query->addField('col', 'field_college_value', 'x_axis');
        $group_by = 'field_college_value';
      } else {
        $query->addField('cor', 'field_course_value', 'x_axis');
        $group_by = 'field_course_value';
      }

      $query->condition('u.field_ucas_year_value', $this->year,'=');

      // limit to selected courses and colleges
      $query->condition('cor.field_course_value',$this->for_courses , 'IN');
      $query->condition('col.field_college_value',$this->for_colleges , 'IN');

    } else { // range of years

      $query->addField('u', 'field_ucas_year_value', 'x_axis');
      $group_by = 'field_ucas_year_value';

      if($this->what == 'college' || $this->what == 'both') {
        $query->condition('col.field_college_value',$this->college , '=');
      }

      if ($this->what == 'course' || $this->what == 'both') {
        $query->condition('cor.field_course_value',$this->course , '=');
      }

    }

    // Get sum of the required values
    $query->addExpression('SUM(tap.field_total_applications_value)', 'sumtap');
    $query->addExpression('SUM(tof.field_total_offers_value)', 'sumtof');
    $query->addExpression('SUM(tac.field_total_acceptances_value)', 'sumtac');

    $result = $query
              ->groupBy($group_by)
              ->execute()
              ->fetchAll();

    return $result;
  }

  public function set_year($param) {
    $this->year = $param;
  }

  public function set_group($param) {
    $this->with_group = $param;
  }

  public function set_what($param) {
    $this->what = $param;
  }

  public function set_colleges($param) {
    $this->for_colleges = $param;
  }

  public function set_courses($param) {
    $this->for_courses = $param;
  }

  public function set_course($param) {
    $this->course = $param;
  }

  public function set_college($param) {
    $this->college = $param;
  }

}
